/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "object-macros.h"
#include "slist.h"

#define R_DECLARE_FIELD_NONE __R_DECLARE_FIELD_NONE
#define R_DECLARE_FIELD(type_name, field_name) \
	__R_DECLARE_FIELD (type_name, field_name)

#define R_DECLARE_VIRTUAL_NONE __R_DECLARE_VIRTUAL_NONE
#define R_DECLARE_VIRTUAL(type_name, return_type, method_name, ...) \
	__R_DECLARE_VIRTUAL (type_name, return_type, method_name, ##__VA_ARGS__)

#define R_DECLARE_SIGNAL_NONE __R_DECLARE_SIGNAL_NONE
#define R_DECLARE_SIGNAL(type_name, signal_name, ...) \
	__R_DECLARE_SIGNAL (type_name, signal_name, ##__VA_ARGS__)

#define R_DECLARE_TYPE_FORWARD(type_name) \
	__R_DECLARE_TYPE_FORWARD (type_name)
#define R_DECLARE_TYPE(type_name, parent_type, fields, virtual_methods, signals) \
	__R_DECLARE_TYPE (type_name, parent_type, fields, virtual_methods, signals)

#define R_IMPLEMENT_VIRTUAL_NONE __R_IMPLEMENT_VIRTUAL_NONE
#define R_IMPLEMENT_VIRTUAL(type_name, method_name, local_name) \
	__R_IMPLEMENT_VIRTUAL (type_name, method_name, local_name)

#define R_IMPLEMENT_SIGNAL_NONE __R_IMPLEMENT_SIGNAL_NONE
#define R_IMPLEMENT_SIGNAL(type_name, signal_name, argument_names, ...) \
	__R_IMPLEMENT_SIGNAL(type_name, signal_name, argument_names, ##__VA_ARGS__)

#define R_IMPLEMENT_TYPE(type_name, parent_type, virtual_methods, signals) \
	__R_IMPLEMENT_TYPE (type_name, parent_type, virtual_methods, signals)

typedef struct _r_object_base r_object_base;
typedef struct _r_object_base_class r_object_base_class;
typedef uint8_t r_signal_id;

struct _r_object_base
{
	r_ref_count ref_count;
	r_object_base_class *c;
	r_slist **signal_slot_lists;
};

struct _r_object_base_class
{
	uint8_t signal_count;
};

extern r_object_base_class *r_object_base_class_id;
extern void r_object_base_construct_class ();
extern r_object_base *r_object_base_cast (r_object_base *self,
										  r_object_base_class *class_id);
extern r_object_base *r_object_base_ref (r_object_base *self);
extern uint8_t r_object_base_unref (r_object_base *self);
extern void r_object_base_construct_begin (r_object_base *self);
extern void r_object_base_construct_end (r_object_base *self);

R_DECLARE_TYPE (r_object, r_object_base,
				R_DECLARE_FIELD_NONE,
				R_DECLARE_VIRTUAL (r_object, void, constructed)
				R_DECLARE_VIRTUAL (r_object, void, finalize)
				R_DECLARE_VIRTUAL (r_object, void, destroy),
				R_DECLARE_SIGNAL_NONE);
