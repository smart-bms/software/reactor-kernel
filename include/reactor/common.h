/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <stdint.h>
#include <stddef.h>

#define R_INIT_LOCAL(name) __attribute__((constructor)) static void __init_local##name ()
#define R_UNUSED __attribute__((unused))
#define R_AUTO_CLEANUP(type_name, cleanup_func) \
	static inline void r_##type_name##_auto_cleanup (type_name *ptr) \
	{ \
		cleanup_func (ptr); \
	}
#define R_AUTOPTR_CLEANUP(type_name, cleanup_func) \
	typedef type_name *type_name##_autoptr_type; \
	static void \
	type_name##_autoptr_cleanup (type_name##_autoptr_type *ptr) \
	{ \
		cleanup_func (*ptr); \
	} \
	R_AUTO_CLEANUP (type_name##_autoptr_type, type_name##_autoptr_cleanup);

#define R_INT(x) ((int) (long) (x))
#define R_UINT(x) ((unsigned int) (unsigned long) (x))
#define R_POINTER(x) ((void *) (unsigned long) (x))

typedef uint16_t r_ref_count;
typedef int (*r_compare_t) (void *v1, void *v2);
typedef void (*r_free_t) (void *);

extern int r_compare_direct (void *v1, void *v2);
extern int r_compare_string (void *v1, void *v2);

#define r_return_if_fail(expr) do { if (!(expr)) { return; } } while (0)
#define r_return_val_if_fail(expr, val) do { if (!(expr)) { return (val); } } while (0)
#define r_auto(type_name) \
	__attribute__((cleanup (r_##type_name##_auto_cleanup))) type_name
#define r_autoptr(type_name) r_auto(type_name##_autoptr_type)
