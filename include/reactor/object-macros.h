/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "common.h"

#define __R_DECLARE_FIELD_NONE
#define __R_DECLARE_FIELD(type_name, field_name) \
	type_name field_name;

#define __R_DECLARE_VIRTUAL_NONE
#define __R_DECLARE_VIRTUAL(type_name, return_type, method_name, ...) \
	return_type (* method_name ) (type_name *self, ##__VA_ARGS__);

#define __R_DECLARE_SIGNAL_NONE
#define __R_DECLARE_SIGNAL(type_name, signal_name, ...) \
	typedef void (*type_name##_##signal_name##_callback) (__VA_ARGS__); \
	extern void type_name##_connect_##signal_name (type_name *self, \
		type_name##_##signal_name##_callback callback); \
	extern void type_name##_emit_##signal_name (type_name *self, ##__VA_ARGS__);

#define __R_DECLARE_TYPE_FORWARD(type_name) \
	typedef struct _##type_name type_name; \
	typedef struct _##type_name##_class type_name##_class;

#define __R_DECLARE_TYPE(type_name, parent_type, fields, virtual_methods, signals) \
	R_DECLARE_TYPE_FORWARD(type_name); \
	extern r_object_base_class *type_name##_class_id; \
	extern void type_name##_construct_class (); \
	struct _##type_name \
	{ \
		parent_type parent; \
		type_name##_class *c; \
		fields \
	}; \
	struct _##type_name##_class \
	{ \
		parent_type##_class parent; \
		virtual_methods \
	}; \
	static inline type_name * \
	type_name##_cast (void *self) \
	{ \
		extern r_object_base *r_object_base_cast (r_object_base *self, \
												  r_object_base_class *class_id); \
		return (type_name *) r_object_base_cast (self, type_name##_class_id); \
	} \
	static inline type_name * \
	type_name##_ref (type_name *self) \
	{ \
		return (type_name *) parent_type##_ref ((parent_type *) self); \
	} \
	static inline uint8_t \
	type_name##_unref (type_name *self) \
	{ \
		return parent_type##_unref ((parent_type *) self); \
	} \
	static inline void \
	type_name##_construct_begin (type_name *self) \
	{ \
		extern void parent_type##_construct_begin (parent_type *self); \
		parent_type##_construct_begin (&self->parent); \
	} \
	static inline void \
	type_name##_construct_end (type_name *self) \
	{ \
		extern void parent_type##_construct_end (parent_type *self); \
		parent_type##_construct_end (&self->parent); \
	} \
	static inline type_name##_class * \
	type_name##_get_class (type_name *self) \
	{ \
		r_object *object; \
		object = r_object_cast (self); \
		if (object != NULL) { \
			return (type_name##_class *) object->parent.c; \
		} \
		return NULL; \
	} \
	static inline uint8_t \
	type_name##_clear (type_name **pself) \
	{ \
		if ((pself) != NULL && type_name##_unref (*(pself))) { \
			*(pself) = NULL; \
			return !0; \
		} \
		return 0; \
	} \
	signals

#define __R_IMPLEMENT_VIRTUAL_NONE
#define __R_IMPLEMENT_VIRTUAL(type_name, method_name, local_name) \
	((type_name##_class *) c)->method_name = local_name;

#define __R_IMPLEMENT_SIGNAL_NONE
#define __R_IMPLEMENT_SIGNAL(type_name, signal_name, argument_names, ...) \
	uint8_t type_name##_##signal_name##_signal_id; \
	R_INIT_LOCAL(type_name##_signal_##signal_name) \
	{ \
		r_object_base_class *c; \
		type_name##_construct_class (); \
		c = type_name##_class_id; \
		type_name##_##signal_name##_signal_id = c->signal_count++; \
	} \
	void type_name##_connect_##signal_name (type_name *self, \
											type_name##_##signal_name##_callback callback) \
	{ \
		r_object_base *base; \
		r_signal_id id; \
		base = (r_object_base *) self; \
		id = type_name##_##signal_name##_signal_id; \
		base->signal_slot_lists[id] = r_slist_prepend (base->signal_slot_lists[id], \
													   callback); \
	} \
	void type_name##_emit_##signal_name (type_name *self, ##__VA_ARGS__) \
	{ \
		type_name##_##signal_name##_callback callback; \
		r_object_base *base; \
		r_signal_id id; \
		r_slist *iter; \
		base = (r_object_base *) self; \
		id = type_name##_##signal_name##_signal_id; \
		iter = base->signal_slot_lists[id]; \
		while (iter != NULL) { \
			callback = iter->data; \
			callback argument_names; \
			iter = iter->next; \
		} \
	}

#define __R_IMPLEMENT_TYPE(type_name, parent_type, overrides, signals) \
	static type_name##_class type_name##_class_instance; \
	r_object_base_class *type_name##_class_id; \
	void type_name##_construct_class () \
	{ \
		R_UNUSED type_name##_class *c; \
		r_return_if_fail (type_name##_class_id == NULL); \
		parent_type##_construct_class (); \
		type_name##_class_id = (r_object_base_class *) &type_name##_class_instance; \
		c = (type_name##_class *) type_name##_class_id; \
		overrides \
	} \
	R_INIT_LOCAL(type_name##_init) \
	{ \
		type_name##_construct_class (); \
	} \
	signals
