/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "object.h"

typedef enum _r_gpio_mode r_gpio_mode;

enum _r_gpio_mode
{
	R_GPIO_MODE_INPUT,
	R_GPIO_MODE_OUTPUT,
};

R_DECLARE_TYPE (r_gpio, r_object,
				R_DECLARE_FIELD_NONE,
				R_DECLARE_VIRTUAL (r_gpio, int, set_mode, uint16_t, r_gpio_mode)
				R_DECLARE_VIRTUAL (r_gpio, int, get_mode, uint16_t, r_gpio_mode *)
				R_DECLARE_VIRTUAL (r_gpio, int, pin_write, uint16_t, uint8_t)
				R_DECLARE_VIRTUAL (r_gpio, int, pin_read, uint16_t, uint8_t *),
				R_DECLARE_SIGNAL (r_gpio, changed, uint16_t));

extern int r_gpio_set_mode (r_gpio *self, uint16_t number, r_gpio_mode mode);
extern int r_gpio_get_mode (r_gpio *self, uint16_t number, r_gpio_mode *mode);
extern int r_gpio_pin_write (r_gpio *self, uint16_t number, uint8_t value);
extern int r_gpio_pin_read (r_gpio *self, uint16_t number, uint8_t *value);
