/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

typedef int (*r_test_t) ();

extern void r_register_test (const char *name, r_test_t test);
extern int r_run_tests ();
