/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "object.h"

R_DECLARE_TYPE_FORWARD (r_stream);

typedef enum _r_priority r_priority;

enum _r_priority
{
	R_PRIORITY_NORMAL,
	R_PRIORITY_HIGH,
};

extern uint64_t r_mono_time ();
extern void *r_reserve (uint16_t size);
extern void r_isr_begin ();
extern void r_isr_end ();
extern uint8_t r_isr_mode ();
extern void r_isr_enable ();
extern void r_isr_disable ();

extern int r_run_once (void (*callback) (void *), void *user_data,
					   uint64_t delay, r_priority priority);
extern int r_run_forever (void (*callback) (void *), void *user_data,
						  uint64_t delay, r_priority priority);
extern int r_run_conditional (int (*callback) (void *), void *user_data,
							  uint64_t delay, r_priority priority);
extern int r_dispatch ();
