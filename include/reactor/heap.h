/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "common.h"

#define R_HEAP_STATIC(name, fixed_capacity, compare_func, free_func) \
	struct { \
		r_heap heap; \
		void *data[fixed_capacity]; \
	} name = { \
		.heap = { \
			.capacity = fixed_capacity, \
			.size = 0, \
			.compare = compare_func, \
			.free_value = free_func, \
		}, \
	};

typedef struct _r_heap r_heap;

struct _r_heap
{
	uint16_t capacity;
	uint16_t size;
	r_compare_t compare;
	r_free_t free_value;

	union {
		void *head;
		void *data[0];
	};
};

extern int r_heap_insert (r_heap *heap, void *value);
extern int r_heap_update_head (r_heap *heap);
extern int r_heap_remove_head (r_heap *heap);
