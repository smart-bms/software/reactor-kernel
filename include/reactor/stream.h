/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "object.h"

R_DECLARE_TYPE (r_stream, r_object,
				R_DECLARE_FIELD_NONE,
				R_DECLARE_VIRTUAL (r_stream, int16_t, write, const void *, uint16_t)
				R_DECLARE_VIRTUAL (r_stream, int16_t, read, void *, uint16_t),
				R_DECLARE_SIGNAL (r_stream, available, int16_t));

extern int16_t r_stream_write (r_stream *self, const void *data, uint16_t bytes);
extern int16_t r_stream_read (r_stream *self, void *data, uint16_t bytes);
