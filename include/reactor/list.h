/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "common.h"

typedef struct _r_list r_list;

struct _r_list
{
	r_list *next;
	r_list *prev;
	void *data;
};

extern void *r_list_data (r_list *list);
extern uint16_t r_list_data_size (r_list *list);
extern r_list *r_list_next (r_list *list);
extern r_list *r_list_prev (r_list *list);
extern r_list *r_list_prepend (r_list *list, void *data);
extern r_list *r_list_append (r_list *list, void *data);
extern r_list *r_list_remove (r_list *list, r_list *node);
extern r_list *r_list_remove_full (r_list *list, r_list *node,
								   void (*destroy) (void *));
extern r_list *r_list_remove_front (r_list *list);
extern r_list *r_list_remove_front_full (r_list *list,
										 void (*destroy) (void *));
extern r_list *r_list_remove_back_full (r_list *list,
										void (*destroy) (void *));
extern r_list *r_list_remove_all (r_list *list);
extern r_list *r_list_remove_all_full (r_list *list,
									   void (*destroy) (void *));
extern uint16_t r_list_length (r_list *list);

R_AUTOPTR_CLEANUP (r_list, r_list_remove_all);
