/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <stdint.h>

extern void *r_slice_alloc	(uint16_t size);
extern void *r_slice_alloc0 (uint16_t size);
extern void *r_slice_copy (const void *mem);
extern void r_slice_free (void *mem);
extern uint16_t r_slice_size (const void *mem);
