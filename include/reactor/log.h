/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#define r_log_d(...) r_log (R_LOG_DEBUG, ##__VA_ARGS__)
#define r_log_m(...) r_log (R_LOG_MESSAGE, ##__VA_ARGS__)
#define r_log_w(...) r_log (R_LOG_WARNING, ##__VA_ARGS__)
#define r_log_e(...) r_log (R_LOG_ERROR, ##__VA_ARGS__)

typedef enum _r_log_level r_log_level;

enum _r_log_level
{
	R_LOG_DEBUG,
	R_LOG_MESSAGE,
	R_LOG_WARNING,
	R_LOG_ERROR,
};

extern void r_log (r_log_level level, const char *fmt, ...)
	__attribute__((format(printf, 2, 3)));
