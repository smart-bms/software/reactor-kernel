/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "common.h"

typedef struct _r_tree r_tree;
typedef int (*r_tree_foreach_callback) (r_tree *tree,
										void *key, void *value,
										void *user_data);

extern r_tree *r_tree_new (r_compare_t compare,
						   r_free_t free_key,
						   r_free_t free_value);
extern r_tree *r_tree_ref (r_tree *tree);
extern r_tree *r_tree_unref (r_tree *tree);
extern int r_tree_foreach (r_tree *tree,
						   r_tree_foreach_callback callback,
						   void *user_data);
extern int r_tree_add (r_tree *tree, void *key);
extern int r_tree_insert (r_tree *tree, void *key, void *value);
extern int r_tree_remove (r_tree *tree, void *key);
extern int r_tree_remove_all (r_tree *tree);
extern int r_tree_contains (r_tree *tree, void *key);
extern int r_tree_lookup_full (r_tree *tree, void *key, void **value);
extern void *r_tree_lookup (r_tree *tree, void *key);
extern void *r_tree_low (r_tree *tree);
extern void *r_tree_low_pair (r_tree *tree, void **value);
