/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "common.h"

typedef struct _r_slist r_slist;

struct _r_slist
{
	r_slist *next;
	void *data;
};

extern r_slist *r_slist_prepend (r_slist *slist, void *data);
extern r_slist *r_slist_remove_front (r_slist *slist);
extern r_slist *r_slist_remove_front_full (r_slist *slist,
										   void (*destroy) (void *));
extern r_slist *r_slist_remove_all (r_slist *slist);
extern r_slist *r_slist_remove_all_full (r_slist *slist,
										 void (*destroy) (void *));
extern r_slist *r_slist_reverse (r_slist *slist);
extern uint16_t r_slist_length (r_slist *slist);

R_AUTOPTR_CLEANUP (r_slist, r_slist_remove_all);
