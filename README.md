# Copyright (c) 2020 Reactor Energy
# Mieszko Mazurek <mimaz@gmx.com>

# Reactor Kernel
This project is the kernel of Reactor BMS firmware. It aims to be thin abstract 
layer over board's hardware and provide common functions, algorithms, structures and
basic object system. By design it's a bit inspired by glib by using slice-like
memory management, GObject-like type system and similar way of creating pseudo
higher level language constructs. Currently it is very experimental sketch of
the final architecture.
