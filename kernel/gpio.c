/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/gpio.h>

static int
set_mode (r_gpio *self, uint16_t number, r_gpio_mode mode)
{
	return -1;
}

static int
get_mode (r_gpio *self, uint16_t number, r_gpio_mode *mode)
{
	return -1;
}

static int
pin_write (r_gpio *self, uint16_t number, uint8_t value)
{
	return -1;
}

static int
pin_read (r_gpio *self, uint16_t number, uint8_t *value)
{
	return -1;
}

R_IMPLEMENT_TYPE (r_gpio, r_object,
				  R_IMPLEMENT_VIRTUAL (r_gpio, set_mode, set_mode)
				  R_IMPLEMENT_VIRTUAL (r_gpio, get_mode, get_mode)
				  R_IMPLEMENT_VIRTUAL (r_gpio, pin_write, pin_write)
				  R_IMPLEMENT_VIRTUAL (r_gpio, pin_read, pin_read),
				  R_IMPLEMENT_SIGNAL (r_gpio, changed, (number), uint16_t number));

int
r_gpio_set_mode (r_gpio *self, uint16_t number, r_gpio_mode mode)
{
	return r_gpio_get_class (self)->set_mode (self, number, mode);
}

int
r_gpio_get_mode (r_gpio *self, uint16_t number, r_gpio_mode *mode)
{
	return r_gpio_get_class (self)->get_mode (self, number, mode);
}

int
r_gpio_pin_write (r_gpio *self, uint16_t number, uint8_t value)
{
	return r_gpio_get_class (self)->pin_write (self, number, value);
}

int
r_gpio_pin_read (r_gpio *self, uint16_t number, uint8_t *value)
{
	return r_gpio_get_class (self)->pin_read (self, number, value);
}
