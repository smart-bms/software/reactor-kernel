/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/object.h>
#include <reactor/slice.h>
#include <reactor/slist.h>

static r_object_base_class base_class;

r_object_base_class *r_object_base_class_id;

void
r_object_base_construct_class ()
{
	r_object_base_class_id = &base_class;
}

r_object_base *
r_object_base_cast (r_object_base *self, r_object_base_class *class_id)
{
	if (self->c == class_id) {
		return self;
	}

	return NULL;
}

r_object_base *
r_object_base_ref (r_object_base *self)
{
	if (self != NULL) {
		self->ref_count++;

		return self;
	}

	return NULL;
}

uint8_t
r_object_base_unref (r_object_base *self)
{
	if (self != NULL) {
		self->ref_count--;

		if (self->ref_count < 1) {
			r_object_get_class ((r_object *) self)->finalize ((r_object *) self);
			r_object_get_class ((r_object *) self)->destroy ((r_object *) self);
		}

		return 1;
	}

	return 0;
}

void
r_object_base_construct_begin (r_object_base *self)
{
	if (self->c->signal_count > 0) {
		self->signal_slot_lists = r_slice_alloc0 (sizeof (r_slist *) * self->c->signal_count);
	}
}

void
r_object_base_construct_end (r_object_base *self)
{
	r_object *object;

	object = r_object_cast (self);

	r_object_get_class (object)->constructed (object);
}

static void
constructed (r_object *self)
{
}

static void
finalize (r_object *self)
{
	r_object_base *base;
	uint8_t i;

	base = (r_object_base *) self;

	for (i = 0; i < base->c->signal_count; i++) {
		r_slist_remove_front (base->signal_slot_lists[i]);
	}

	if (base->c->signal_count > 0) {
		r_slice_free (base->signal_slot_lists);
	}
}

static void
destroy (r_object *self)
{
	r_slice_free (self);
}

R_IMPLEMENT_TYPE (r_object, r_object_base,
				  R_IMPLEMENT_VIRTUAL (r_object, constructed, constructed)
				  R_IMPLEMENT_VIRTUAL (r_object, finalize, finalize)
				  R_IMPLEMENT_VIRTUAL (r_object, destroy, destroy),
				  R_IMPLEMENT_SIGNAL_NONE);
