/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/slice.h>
#include <reactor/kernel.h>
#include <reactor/log.h>

#include <string.h>

#define DEBUG 1

struct block
{
	union {
		uint16_t size;
		struct block *next;
	};

	uint8_t data[0];
};

static struct block *roots[64];

static void
find_root (uint16_t *size, uint8_t *index)
{
	*index = (*size + sizeof (void *) - 1) / sizeof (void *);
	*size = *index * sizeof (void *);
}

void *
r_slice_alloc (uint16_t size)
{
	struct block *b;
	uint8_t index;

	find_root (&size, &index);

#if DEBUG
	r_log_d ("slice alloc %d", size);
#endif
	r_isr_disable ();

	if (roots[index] == NULL) {
		b = r_reserve (size + sizeof (struct block));
	} else {
		b = roots[index];
		roots[index] = b->next;
	}

	b->size = size;

	r_isr_enable ();

	return b->data;
}

void *
r_slice_alloc0 (uint16_t size)
{
	void *mem;

	mem = r_slice_alloc (size);
	memset (mem, 0, size);

	return mem;
}

void *
r_slice_copy (const void *mem)
{
	void *copy;

	copy = r_slice_alloc (r_slice_size (mem));
	memcpy (copy, mem, r_slice_size (mem));

	return copy;
}

void
r_slice_free (void *mem)
{
	struct block *b;
	uint16_t size;
	uint8_t index;

	size = r_slice_size (mem);
	find_root (&size, &index);

	b = (struct block *) ((uint8_t *) mem - sizeof (struct block));
#if DEBUG
	r_log_d ("slice free %d", size);
#endif

	r_isr_disable ();

	b->next = roots[index];
	roots[index] = b;

	r_isr_enable ();
}

uint16_t
r_slice_size (const void *mem)
{
	return ((struct block *) ((const uint8_t *) mem - sizeof (struct block)))->size;
}
