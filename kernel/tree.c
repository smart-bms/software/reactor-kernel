/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/tree.h>
#include <reactor/kernel.h>
#include <reactor/slice.h>

#include <string.h>

enum color
{
	RED,
	BLACK,
};

struct node
{
	void *key;
	void *value;
	struct node *left;
	struct node *right;
	enum color color;
};

struct _r_tree
{
	struct node *root;
	r_compare_t compare;
	r_free_t free_key;
	r_free_t free_value;
	r_ref_count ref_count;
};

static struct node *
find_node (r_tree *tree, void *key)
{
	struct node *node;
	int result;

	node = tree->root;

	while (node != NULL) {
		result = tree->compare (key, node->key);

		if (result < 0) {
			node = node->left;
		} else if (result > 0) {
			node = node->right;
		} else {
			return node;
		}
	}

	return NULL;
}

r_tree *
r_tree_new (r_compare_t compare,
			r_free_t free_key,
			r_free_t free_value)
{
	r_tree *tree;

	tree = r_slice_alloc (sizeof (r_tree));
	tree->root = NULL;
	tree->compare = compare;
	tree->free_key = free_key;
	tree->free_value = free_value;
	tree->ref_count = 1;

	return tree;
}

r_tree *
r_tree_ref (r_tree *tree)
{
	tree->ref_count++;

	return tree;
}

r_tree *
r_tree_unref (r_tree *tree)
{
	tree->ref_count--;

	if (tree->ref_count < 1) {
		r_tree_remove_all (tree);
		r_slice_free (tree);
	}

	return NULL;
}

int
r_tree_foreach (r_tree *tree, r_tree_foreach_callback callback,
				void *user_data)
{
	/*
	 * TODO iterate over collection
	 */

	return 0;
}

int
r_tree_add (r_tree *tree, void *key)
{
	return r_tree_insert (tree, key, key);
}

int
r_tree_insert (r_tree *tree, void *key, void *value)
{
	/*
	 * TODO insert node here
	 */

	return 0;
}

int
r_tree_remove (r_tree *tree, void *key)
{
	struct node *node;

	node = find_node (tree, key);

	r_return_val_if_fail (node != NULL, 0);

	/*
	 * TODO remove node here
	 */

	return 1;
}

static void
remove_recursive (r_tree *tree, struct node *node)
{
	r_return_if_fail (node != NULL);

	remove_recursive (tree, node->left);
	remove_recursive (tree, node->right);

	if (tree->free_key != NULL) {
		tree->free_key (node->key);
	}

	if (tree->free_value != NULL) {
		tree->free_value (node->value);
	}

	r_slice_free (node);
}

int
r_tree_remove_all (r_tree *tree)
{
	r_return_val_if_fail (tree->root != NULL, 0);

	remove_recursive (tree, tree->root);
	tree->root = NULL;

	return 1;
}

int
r_tree_contains (r_tree *tree, void *key)
{
	return r_tree_lookup_full (tree, key, NULL);
}

int
r_tree_lookup_full (r_tree *tree, void *key, void **value)
{
	struct node *node;

	node = find_node (tree, key);

	if (node != NULL) {
		if (value != NULL) {
			*value = node->value;
		}

		return 1;
	}

	return 0;
}

void *
r_tree_lookup (r_tree *tree, void *key)
{
	void *value;

	r_tree_lookup_full (tree, key, &value);

	return value;
}

void *
r_tree_low (r_tree *tree)
{
	return r_tree_low_pair (tree, NULL);
}

void *
r_tree_low_pair (r_tree *tree, void **value)
{
	struct node *node;

	node = tree->root;

	r_return_val_if_fail (node != NULL, NULL);

	while (node->left != NULL) {
		node = node->left;
	}

	if (value != NULL) {
		*value = node->value;
	}

	return node->key;
}
