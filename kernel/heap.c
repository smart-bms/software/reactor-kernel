/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/heap.h>

R_HEAP_STATIC (test, 50, r_compare_direct, NULL);

static void bubble_up (r_heap *heap);
static void bubble_down (r_heap *heap);

int
r_heap_insert (r_heap *heap, void *value)
{
	r_return_val_if_fail (heap->size < heap->capacity, 0);

	heap->data[heap->size++] = value;

	bubble_down (heap);

	return 1;
}

int
r_heap_update_head (r_heap *heap)
{
	void *head;

	head = heap->head;

	bubble_up (heap);

	return heap->head != head;
}

int
r_heap_remove_head (r_heap *heap)
{
	r_return_val_if_fail (heap->size > 0, 0);

	if (heap->free_value != NULL) {
		heap->free_value (heap->head);
	}

	heap->data[0] = heap->data[--heap->size];

	bubble_up (heap);

	return 1;
}

static void
bubble_up (r_heap *heap)
{
	int index, cmp, next0, next1, next;
	void *tmp;

	index = 0;
	next0 = index * 2 + 1;

	while (next0 < heap->size) {
		next1 = next0 + 1;

		if (next1 < heap->size) {
			cmp = heap->compare (heap->data[next0], heap->data[next1]);

			if (cmp < 0) {
				next = next0;
			} else {
				next = next1;
			}
		} else {
			next = next0;
		}

		cmp = heap->compare (heap->data[index], heap->data[next]);

		if (cmp < 0) {
			return;
		}

		tmp = heap->data[index];
		heap->data[index] = heap->data[next];
		heap->data[next] = tmp;

		index = next;
		next0 = index * 2 + 1;
	}
}

static void
bubble_down (r_heap *heap)
{
}
