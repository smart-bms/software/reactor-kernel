/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/kernel.h>
#include <reactor/test.h>
#include <reactor/log.h>
#include <glib.h>
#include <stdio.h>

void
r_log (r_log_level level, const char *fmt, ...)
{
	GLogLevelFlags flags;
	va_list args;

	switch (level) {
	case R_LOG_DEBUG:
		flags = G_LOG_LEVEL_DEBUG;
		break;

	case R_LOG_MESSAGE:
		flags = G_LOG_LEVEL_MESSAGE;
		break;

	case R_LOG_WARNING:
		flags = G_LOG_LEVEL_WARNING;
		break;

	case R_LOG_ERROR:
		flags = G_LOG_LEVEL_ERROR;
		break;

	default:
		g_error ("invalid log level: %d", level);
		return;
	}

	va_start (args, fmt);
	g_logv (NULL, flags, fmt, args);
	va_end (args);
}

uint64_t
r_mono_time ()
{
	return g_get_monotonic_time ();
}

void *
r_reserve (uint16_t size)
{
	return g_malloc (size);
}

void
r_isr_disable ()
{
}

void
r_isr_enable ()
{
}

/* __attribute__((constructor)) */
/* static void */
/* init_something () */
R_INIT_LOCAL (foo)
{
	g_message ("init_something");
}

int main ()
{
	g_autoptr (GMainLoop) loop;

	loop = g_main_loop_new (NULL, FALSE);

	g_timeout_add (1000, (GSourceFunc) r_dispatch, NULL);

	r_log (R_LOG_MESSAGE, "hello, reactor!");
	r_run_tests ();

	/* g_main_loop_run (loop); */

	return 0;
}
