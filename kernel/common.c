/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/common.h>

#include <string.h>

int
r_compare_direct (void *k1, void *k2)
{
	if (k1 > k2) {
		return 1;
	} else if (k1 < k2) {
		return -1;
	}

	return 0;
}

int
r_compare_string (void *k1, void *k2)
{
	return strcmp (k1, k2);
}

