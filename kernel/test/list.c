/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/list.h>
#include <reactor/test.h>
#include <reactor/log.h>

#define r_log_d

static int
list_test ()
{
	r_autoptr (r_list) list = NULL;
	r_list *iter;
	int i;

	for (i = 0; i < 10; i++) {
		list = r_list_prepend (list, R_POINTER (i));
	}

	for (i = 0; i < 10; i++) {
		list = r_list_append (list, R_POINTER (i));
	}

	iter = list;

	for (i = 0; i < 20; i++) {
		r_log_d ("iter %d", R_INT (iter->data));

		if (i < 10) {
			r_return_val_if_fail (R_INT (iter->data) == 9 - i, 0);
		} else {
			r_return_val_if_fail (R_INT (iter->data) == i - 10, 0);
		}

		iter = iter->next;
	}

	r_return_val_if_fail (iter == list, 0);

	return 1;
}

R_INIT_LOCAL (slist)
{
	r_register_test ("list", list_test);
}
