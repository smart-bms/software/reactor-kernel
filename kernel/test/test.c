/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/kernel.h>
#include <reactor/test.h>
#include <reactor/log.h>

struct test
{
	r_test_t func;
	const char *name;
};

static struct test test_v[64];
static int test_c;

void
r_register_test (const char *name, r_test_t test)
{
	test_v[test_c].func = test;
	test_v[test_c++].name = name;
}

int
r_run_tests ()
{
	int i, fails;

	fails = 0;
	r_log (R_LOG_MESSAGE, "test count %d", test_c);

	for (i = 0; i < test_c; i++) {
		if (test_v[i].func ()) {
			r_log (R_LOG_MESSAGE, "test %s passed", test_v[i].name);
		} else {
			r_log (R_LOG_ERROR, "test %s failed", test_v[i].name);
			fails++;
		}
	}

	if (fails > 0) {
		r_log_e ("%d tests failed", fails);
		return 0;
	}

	return 1;
}
