/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/slist.h>
#include <reactor/test.h>
#include <reactor/log.h>

#define r_log_d(...)

static int
slist_test ()
{
	r_autoptr (r_slist) list = NULL;
	r_slist *iter;
	int i;

	list = NULL;

	for (i = 0; i < 10; i++) {
		list = r_slist_prepend (list, R_POINTER (i));
	}

	iter = list;

	for (i = 0; i < 10; i++) {
		r_log_d ("value %d %d", i, R_INT (iter->data));

		r_return_val_if_fail (i == 9 - R_INT (iter->data), 0);
		iter = iter->next;
	}

	r_log_d ("NULL: %p", iter);
	r_return_val_if_fail (iter == NULL, 0);

	list = r_slist_reverse (list);
	iter = list;

	for (i = 0; i < 10; i++) {
		r_log_d ("value %d %d", i, R_INT (iter->data));

		r_return_val_if_fail (i == R_INT (iter->data), 0);
		iter = iter->next;
	}

	r_log_d ("NULL: %p", iter);
	r_return_val_if_fail (iter == NULL, 0);

	return 1;
}

R_INIT_LOCAL (slist)
{
	r_register_test ("slist", slist_test);
}
