/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/list.h>
#include <reactor/slice.h>
#include <reactor/log.h>

#include <string.h>

static r_list *
append_node (r_list *prev, void *data)
{
	r_list *next, *node;

	node = r_slice_alloc (sizeof (r_list));
	node->data = data;

	if (prev != NULL) {
		next = prev->next;

		prev->next = node;
		next->prev = node;
	} else {
		r_log_d ("append prev null");
		prev = node;
		next = node;
	}

	node->next = next;
	node->prev = prev;

	return node;
}

static void
remove_node (r_list *node, void (*destroy) (void *))
{
	r_list *prev, *next;

	r_return_if_fail (node != NULL);

	prev = node->prev;
	next = node->next;

	if (prev != NULL) {
		prev->next = next;
	}

	if (next != NULL) {
		next->prev = prev;
	}

	if (destroy != NULL) {
		destroy (node->data);
	}

	r_slice_free (node);
}

void *
r_list_data (r_list *list)
{
	return list->data;
}

uint16_t
r_list_data_size (r_list *list)
{
	return r_slice_size (list) - sizeof (struct _r_list);
}

r_list *
r_list_next (r_list *list)
{
	return list->next;
}

r_list *
r_list_prev (r_list *list)
{
	return list->prev;
}

r_list *
r_list_prepend (r_list *list, void *data)
{
	if (list != NULL) {
		return append_node (list->prev, data);
	}

	return append_node (NULL, data);
}

r_list *
r_list_append (r_list *list, void *data)
{
	if (list != NULL) {
		append_node (list->prev, data);

		return list;
	}

	return append_node (NULL, data);
}

r_list *
r_list_remove (r_list *list, r_list *node)
{
	return r_list_remove_full (list, node, NULL);
}

r_list *
r_list_remove_full (r_list *list, r_list *node,
					void (*destroy) (void *))
{
	r_list *root;

	if (node != list) {
		root = list;
	} else if (list->next != list) {
		root = list->next;
	} else {
		root = NULL;
	}

	remove_node (node, destroy);

	return root;
}

r_list *
r_list_remove_front (r_list *list)
{
	return r_list_remove_front_full (list, NULL);
}

r_list *
r_list_remove_front_full (r_list *list, void (*destroy) (void *))
{
	return r_list_remove_full (list, list, destroy);
}

r_list *
r_list_remove_back (r_list *list)
{
	return r_list_remove_back_full (list, NULL);
}

r_list *
r_list_remove_back_full (r_list *list, void (*destroy) (void *))
{
	r_return_val_if_fail (list != NULL, NULL);

	return r_list_remove_full (list, list->prev, destroy);
}

r_list *
r_list_remove_all (r_list *list)
{
	return r_list_remove_all_full (list, NULL);
}

r_list *
r_list_remove_all_full (r_list *list,
						void (*destroy) (void *))
{
	while (list != NULL) {
		list = r_list_remove_full (list, list, destroy);
	}

	return list;
}

uint16_t
r_list_length (r_list *list)
{
	r_list *iter;
	uint16_t length;

	iter = list;
	length = 0;

	while (iter != NULL) {
		length++;
		iter = iter->next;

		if (iter == list) {
			break;
		}
	}

	return length;
}
