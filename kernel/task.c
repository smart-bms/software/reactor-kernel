/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/kernel.h>
#include <reactor/tree.h>
#include <reactor/slice.h>

enum run_mode
{
	MODE_ONCE,
	MODE_FOREVER,
	MODE_CONDITIONAL,
};

struct run_task
{
	int64_t time;
	int64_t period;
	int (*callback) (void *);
	void *user_data;
	enum run_mode mode;
};

static int compare_task (void *v1, void *v2);
static int run_full (int (*callback) (void *), void *user_data,
					 uint64_t delay, r_priority priority,
					 enum run_mode mode);
static int dispatch_tree (r_tree *tree);

static r_tree *normal_tree;
static r_tree *high_tree;

R_INIT_LOCAL (tree_init)
{
	normal_tree = r_tree_new (compare_task,
							  NULL, NULL);
	high_tree = r_tree_new (compare_task,
							NULL, NULL);
}

int
r_run_once (void (*callback) (void *), void *user_data,
			uint64_t delay, r_priority priority)
{
	return run_full ((int (*) (void *)) callback, user_data,
					 delay, priority, MODE_ONCE);
}

int
r_run_forever (void (*callback) (void *), void *user_data,
			   uint64_t delay, r_priority priority)
{
	return run_full ((int (*) (void *)) callback, user_data,
					 delay, priority, MODE_FOREVER);
}

int
r_run_conditional (int (*callback) (void *), void *user_data,
				   uint64_t delay, r_priority priority)
{
	return run_full (callback, user_data,
					 delay, priority, MODE_CONDITIONAL);
}

int
r_dispatch ()
{
	return dispatch_tree (high_tree) || dispatch_tree (normal_tree);
}

static int
compare_task (void *v1, void *v2)
{
	struct run_task *t1 = v1;
	struct run_task *t2 = v2;

	if (t1->time < t2->time) {
		return -1;
	} else if (t1->time > t2->time) {
		return 1;
	}

	return 0;
}

static int
run_full (int (*callback) (void *), void *user_data,
		  uint64_t delay, r_priority priority,
		  enum run_mode mode)
{
	struct run_task *task;
	r_tree *tree;
	int64_t time;

	time = r_mono_time () + delay;

	switch (priority) {
	case R_PRIORITY_NORMAL:
		tree = normal_tree;
		break;

	case R_PRIORITY_HIGH:
		tree = high_tree;
		break;

	default:
		return 0;
	}

	task = r_slice_alloc (sizeof (struct run_task));
	task->time = time;
	task->period = delay;
	task->callback = callback;
	task->user_data = user_data;
	task->mode = mode;

	r_tree_add (tree, task);

	return 1;
}

static int
dispatch_tree (r_tree *tree)
{
	struct run_task *task;
	int repeat;

	task = r_tree_low (tree);

	while (task != NULL && task->time <= r_mono_time ()) {
		r_tree_remove (tree, task);

		repeat = task->callback (task->user_data);

		switch (task->mode) {
		case MODE_ONCE:
			repeat = 0;
			break;
			
		case MODE_FOREVER:
			repeat = 1;
			break;

		case MODE_CONDITIONAL:
			break;
		}

		if (repeat) {
			task->time += task->period;
			r_tree_add (tree, task);
		} else {
			r_slice_free (task);
		}

		task = r_tree_low (tree);
	}

	return 1;
}
