/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/slist.h>
#include <reactor/slice.h>
#include <reactor/log.h>

#include <stdlib.h>

r_slist *
r_slist_prepend (r_slist *slist, void *data)
{
	r_slist *head;

	head = r_slice_alloc (sizeof (r_slist));
	head->next = slist;
	head->data = data;

	return head;
}

r_slist *
r_slist_remove_front (r_slist *slist)
{
	return r_slist_remove_front_full (slist, NULL);
}

r_slist *
r_slist_remove_front_full (r_slist *slist,
						   void (*destroy) (void *))
{
	r_slist *next;

	r_return_val_if_fail (slist != NULL, NULL);

	next = slist->next;

	if (destroy != NULL) {
		destroy (slist->data);
	}

	r_slice_free (slist);

	return next;
}

r_slist *
r_slist_remove_all (r_slist *slist)
{
	return r_slist_remove_all_full (slist, NULL);
}

r_slist *
r_slist_remove_all_full (r_slist *slist, void (*destroy) (void *))
{
	while (slist != NULL) {
		slist = r_slist_remove_front_full (slist, destroy);
	}

	return slist;
}

r_slist *
r_slist_reverse (r_slist *slist)
{
	r_slist *reversed, *next;

	reversed = NULL;

	while (slist != NULL) {
		next = slist->next;
		slist->next = reversed;
		reversed = slist;
		slist = next;
	}

	return reversed;
}

uint16_t
r_slist_length (r_slist *slist)
{
	uint16_t length;

	length = 0;

	while (slist != NULL) {
		length++;
		slist = slist->next;
	}

	return length;
}
