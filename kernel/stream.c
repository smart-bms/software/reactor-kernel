/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <reactor/stream.h>
#include <reactor/slice.h>

#include <string.h>

static int16_t
write (r_stream *self, const void *data, uint16_t bytes)
{
	return -1;
}

static int16_t
read (r_stream *self, void *data, uint16_t bytes)
{
	return -1;
}

int16_t
r_stream_write (r_stream *self, const void *data, uint16_t bytes)
{
	return r_stream_get_class (self)->write (self, data, bytes);
}

int16_t
r_stream_read (r_stream *self, void *data, uint16_t bytes)
{
	return r_stream_get_class (self)->read (self, data, bytes);
}

R_IMPLEMENT_TYPE (r_stream, r_object,
				  R_IMPLEMENT_VIRTUAL (r_stream, write, write)
				  R_IMPLEMENT_VIRTUAL (r_stream, read, read),
				  R_IMPLEMENT_SIGNAL (r_stream, available, (bytes), int16_t bytes));
